package hwo2014

class RaceState(pieces: List[TrackPiece], lanes: List[TrackLane]) {
	import scala.collection.mutable.Map

	var throttleMapping = pieces.map(x => 0.7).toArray

	var carPosition = Map[CarId, PiecePosition]()
	var carSpeed = Map[CarId, Double]()

	def setCarPosition(carId: CarId, newPos: PiecePosition) = {

		val oldPos = carPosition.get(carId)
		val speed = oldPos match {
			case Some(oPos) =>
				if (oPos.pieceIndex != newPos.pieceIndex) {
					val oldLength = pieces(oPos.pieceIndex).getLength
					val inOld = oldLength-oPos.inPieceDistance
					val inNew = newPos.inPieceDistance
					println("Old Length: " + oldLength)
					println("In old: " + inOld)
					println("In new: " + inNew)
					(inOld
						+ inNew)
				} else {
					newPos.inPieceDistance - oPos.inPieceDistance
				}

			case None =>
				newPos.inPieceDistance
				}
		println("Speed: " + speed)

		carSpeed.put(carId, speed)

		carPosition.update(carId, newPos)
		}


	def preCalc = {
		def h(p: TrackPiece): Double = {
			/*val re = p match {
				case TrackPiece(Some(radius), Some(angle), _, _ ) => 
					val daempfFaktor =  Math.abs( (angle / 360) / radius) 
					println("Angle: " + angle + " radius: " + radius)
					0.8 / (1 + daempfFaktor )
				case _ => 0.8
	}

		//Runden auf 2 Stellen
		re - (re % 0.01)*/
	       0.9
			}

		throttleMapping = (for {
			piece <- pieces
		} yield h(piece)).toArray
		}
	preCalc
	throttleMapping.toList.foreach(println)

	def byIndex(index: Int): Double = throttleMapping(index)

	def reduceThrottle(index: Int, newValue: Double): Unit = {
		val i = index % pieces.length

		println("i: " + i)

		if (throttleMapping(i) == 0) {
			reduceThrottle(i - 1, 0)
		} else {
			throttleMapping(i) = Math.max(throttleMapping(i) - 0.7, 0)
		}
		}

	}
