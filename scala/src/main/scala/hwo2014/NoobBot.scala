package hwo2014

import org.json4s._
import org.json4s.DefaultFormats
import java.net.Socket
import java.io.{ BufferedReader, InputStreamReader, OutputStreamWriter, PrintWriter }
import org.json4s.native.Serialization
import scala.annotation.tailrec

object NoobBot extends App {
	args.toList match {
		case hostName :: port :: botName :: botKey :: _ =>
			new NoobBot(hostName, Integer.parseInt(port), botName, botKey)
		case _ => println("args missing")
}
	}

class NoobBot(host: String, port: Int, botName: String, botKey: String) {
	implicit val formats = new DefaultFormats {}
	val socket = new Socket(host, port)
	val writer = new PrintWriter(new OutputStreamWriter(socket.getOutputStream, "UTF8"))
	val reader = new BufferedReader(new InputStreamReader(socket.getInputStream, "UTF8"))

	var raceState: RaceState = null
	var myCarId: CarId = null

	send(MsgWrapper("join", Join(botName, botKey)))
	play

	private def play {
		val line = reader.readLine()
		if (line != null) {
			val msgWrapper = Serialization.read[MsgWrapper](line)

			try {
				val m = this.getClass().getMethod("on" + msgWrapper.msgType.capitalize, classOf[JValue])
				m.invoke(this, msgWrapper.data)
			} catch {
				case _ : Throwable=>
					println("Received: " + msgWrapper.msgType)
			}

			play

			}
		}

	def onError(data: JValue) = {
		println("Error: " + data)
	}

	def onGameInit(data: JValue) = {
		val info = data.extract[GameInit]

		println("Length: " + info.race.track.pieces.length)

		raceState = new RaceState(info.race.track.pieces, info.race.track.lanes)
		info.race.track.pieces.foreach(println)
	}

	def onYourCar(data: JValue) = {
		myCarId = data.extract[CarId]
	}

	def onSpawn(data: JValue) = {
		println("Spawn")
		send(MsgWrapper("throttle", 1))
	}

	def onCrash(data: JValue) = {
		println("Crash")
		var carid = data.extract[CarId]
		val latestPost = raceState.carPosition.get(carid)

		//reduce speed in latest
		val latestPieceIndex = latestPost.get.pieceIndex - 1
		println("latest: " + latestPieceIndex)
		raceState.reduceThrottle(latestPieceIndex, 0)
	}

	def onCarPositions(data: JValue) = {
		val carPos = data.extract[List[CarPosition]]
		val carsById = carPos.map(x => (x.id, x.piecePosition)).toMap
		val myPos = carsById.get(myCarId).get

		carsById.foreach {
			case (id, pos) => raceState.setCarPosition(id, pos)
	}

	val throttle = raceState.byIndex(myPos.pieceIndex)
	println(myPos.pieceIndex + " " + throttle)

	send(MsgWrapper("throttle", throttle))
	//println(carPos)
		}

	def send(msg: MsgWrapper) {
		writer.println(Serialization.write(msg))
		writer.flush
	}

	}

case class RaceSessionInfo(laps: Int, maxLapTimeMs: Int, quickRace: Boolean)
case class RaceInfoWrapper(track: TrackInfo, cars: List[CarInfo], raceSession: RaceSessionInfo)
case class TrackInfo(id: String, name: String, pieces: List[TrackPiece], lanes: List[TrackLane], startingPoint: StartingPoint)
class TrackPiece(radius: Option[Double], angle: Option[Double], length: Option[Double], switch: Option[Boolean]){
	def getLength() : Double = {
		if(radius.isDefined) Math.toRadians(angle.get) * radius.get
		else 
			length.get

}
	}
case class TrackLane(distanceFromCenter: Int, index: Int)
case class StartingPoint(position: Position, angle: Double)
case class Position(x: Double, y: Double)
case class CarInfo(id: CarId, dimensions: CarDimensions)
case class CarId(name: String, color: String)
case class CarDimensions(length: Double, width: Double, guideFlagPosition: Double)
case class GameInit(race: RaceInfoWrapper)
case class LaneInfo(startLaneIndex: Int, endLaneIndex: Int)
case class PiecePosition(pieceIndex: Int, inPieceDistance: Double, lane: LaneInfo, lap: Int)
case class CarPosition(id: CarId, angle: Double, piecePosition: PiecePosition)
case class Join(name: String, key: String)
case class MsgWrapper(msgType: String, data: JValue)

object MsgWrapper {
	implicit val formats = new DefaultFormats {}

	def apply(msgType: String, data: Any): MsgWrapper = {
		MsgWrapper(msgType, Extraction.decompose(data))
	}
	}

